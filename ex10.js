console.log(html`<b>${process.argv[2]} says</b>: "${process.argv[3]}"`);

function html(name, ...comments) {
    var final = name[0];
    for (var i = 0; i < comments.length; ++i) {
        final += seq(comments[i]) + name[i + 1];
    }

    return final;
}


function seq(e) {
    return e.replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/'/g, "&apos;")
}